﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagnetronD3
{
    public partial class Form1 : Form
    {
        protected bool MicrowaveStatus;
        protected static bool Door;

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The function down below lets you exit the application throught the menu
        /// </summary>

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Timer with interval = 1 second
        /// </summary>
        
        private void timer_Tick(object sender, EventArgs e)
        {
            if (MicrowaveStatus)
            {
                MicrowaveTimer.Instance.CountDown();
                microwaveTimer.Text = string.Format("{0}:{1}", MicrowaveTimer.Instance.Minute.ToString().PadLeft(2, '0'), MicrowaveTimer.Instance.Second.ToString().PadLeft(2, '0'));
            }
        }
        /// <summary>
        /// In the function down below is the time to timer added in seconds
        /// </summary>
       
        private void buttonAdd5Min(object sender, EventArgs e)
        {
            AddTimeTimer(300);
        }

        private void buttonAdd10Min(object sender, EventArgs e)
        {
            AddTimeTimer(600);
        }

        /// <summary>
        ///  The funtion down below lets you control the microwave door
        /// </summary>
        private void ButtonDoor(object sender, EventArgs e)
        {
            switch (Door)
            {
                case false:
                    Door = true;
                    MicrowaveStatus = false;
                    pictureBox1.Image = Properties.Resources.open_microwave1;
                    break;
                case true:
                    Door = false;
                    MicrowaveStatus = true;
                    pictureBox1.Image = Properties.Resources.microwave1;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// In the function down below you can start the  microwave with fast 30 seconds 
        /// If microwave is paused, this method will continue the timer
        /// </summary>
        private void ButtonStartAddSeconds(object sender, EventArgs e)
        {
            if (MicrowaveStatus || (MicrowaveTimer.Instance.Minute == 0 && MicrowaveTimer.Instance.Second == 0))
            {
                MicrowaveTimer.Instance.AddSeconds(30);
                pictureBox1.Image = Properties.Resources.microwavelight1;
            }
            if (!Door)
            {
                MicrowaveStatus = true;
            }
        }

        /// <summary>
        /// Button to pause or if already paused, clearing function
        /// </summary>
        private void ButtonPauseClear(object sender, EventArgs e)
        {
            if (!MicrowaveStatus)
            {
                MicrowaveTimer.Instance.CounterReset();
                microwaveTimer.Text = "00:00";
                pictureBox1.Image = Properties.Resources.microwave1;
            }
            MicrowaveStatus = false;
        }

        /// <summary>
        /// Time adder 
        /// </summary>
 
        private void AddTimeTimer(int seconds)
        {
            MicrowaveTimer.Instance.AddSeconds(seconds);
            MicrowaveStatus = true;
        }

        private void microwaveTimer_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
